/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.buscaminasgold;

/**
 *
 * @author Roberto
 */
public class BM_Celda {
    
    public  boolean hayMina;
    public  byte minasAlrededor;
    public  byte estadoDeCeldas;
    
    public static final byte DESTAPADA = 1;
    public static final byte CUBIERTA = 2;
    public static final byte BANDERITA = 3;
    
    BM_Celda(){
       
       hayMina = false;
       minasAlrededor = 0;
       estadoDeCeldas = CUBIERTA;
   
   }
    
    //Metodos get y set de si hayMina
    public void setMinas (boolean hayMina){
        this.hayMina = hayMina;
    }
    
    public boolean getMina () {
        return hayMina;
    }
    
    //Metodos get y set de las minas alrededor
    public void setAlrededor (byte minasAlrededor){
        this.minasAlrededor = minasAlrededor;
    }
    
    public byte getAlrededor () {
        return minasAlrededor;
    }
    
    public void setEstado (byte estadoDeCeldas){
        this.estadoDeCeldas = estadoDeCeldas;
    }
    
    public byte getEstado() {
        return estadoDeCeldas;
    }
    
    
    
}
