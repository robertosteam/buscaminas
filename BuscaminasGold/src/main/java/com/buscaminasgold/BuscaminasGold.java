package com.buscaminasgold;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class BuscaminasGold extends Application {

            GestorInfo info = new GestorInfo(10, 10 ,1);

    @Override
    public void start(Stage stage) {
        
        
        //Creacion de la ventana principal para comprobar el funcionamiento.
        HBox ventana = new HBox();
        Button botonComprobar = new Button("Comprobar");
        Button botonBanderita = new Button("Colocar bandera");
        TextField posicionX = new TextField();
        TextField posicionY = new TextField();
        ventana.getChildren().addAll(botonComprobar,botonBanderita , posicionX, posicionY);
        ventana.setAlignment(Pos.CENTER);

        // Creacion del boton para destapar la casilla
        botonComprobar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int numeroFila = Integer.valueOf(posicionX.getText());
                int numeroColumna = Integer.valueOf(posicionY.getText());
                info.descubrirCelda(numeroFila, numeroColumna); 
               System.out.println(info.toString());
            }
        });
        
        // Creacion del boton para colocar la banderita
        botonBanderita.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int numeroFila = Integer.valueOf(posicionX.getText());
                int numeroColumna = Integer.valueOf(posicionY.getText());
                info.colocarBandera(numeroFila, numeroColumna);
            }
        });
        
        
        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
        Scene scene = new Scene(ventana, visualBounds.getWidth(), visualBounds.getHeight());

        stage.getIcons().add(new Image(BuscaminasGold.class.getResourceAsStream("/icon.png")));

        stage.setScene(scene);
        stage.show();
        
        
        //System.out.println(info.descubrirCelda());
        //System.out.println(info.toString());
    }

}
